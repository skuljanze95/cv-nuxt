export default {
  // Target: https://go.nuxtjs.dev/config-target
  target: "static",

  // Global App headers: https://go.nuxtjs.dev/config-head
  head: {
    title: "Anže Škulj",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: "Experienced Frontend Developer with a demonstrated history of working in the computer software industry. Skilled in Vue, Flutter, HTML, CSS, Javascript Typescript, Dart.",
      },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
  },
  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ["~assets/styles/app.scss"],
  styleResources: {
    scss: ["~/assets/styles/app.scss"],
  },
  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [ '~plugins/vue-js-modal.js'],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: ["@nuxtjs/google-fonts", "nuxt-animejs"],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    "@nuxtjs/style-resources",
    [
      '@nuxtjs/firebase',
      {
        config: {
          apiKey: "AIzaSyBfAWE0Pqk0LVaN1a8NEv2h0FGMfOrGtmI",
          authDomain: "anze-cv-b7849.firebaseapp.com",
          databaseURL: "https://anze-cv-b7849.firebaseio.com",
          projectId: "anze-cv-b7849",
          storageBucket: "anze-cv-b7849.appspot.com",
          messagingSenderId: "1029422667633",
          appId: "1:1029422667633:web:2aedecc42631045b5848d0",
          measurementId: "G-8FHVRJTLHY"
        },
        services: {
          firestore:{
            memoryOnly: false,
            enablePersistence: true,
          },
          analytics: true,
        }
      }
    ]
  ],

  googleFonts: {
    display: "swap",
    families: {
      Nunito: [300, 400, 600, 700],
    },
  },
};
